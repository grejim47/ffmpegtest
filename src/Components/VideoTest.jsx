import React from 'react';
import { createFFmpeg, fetchFile } from '@ffmpeg/ffmpeg';
import { useEffect } from 'react';
const ffmpeg = createFFmpeg(
    {
        log: true
    }
)

const VideoTest = () => {

    const [ready, setReady] = React.useState(false);
    const [video, setVideo] = React.useState();
    const [gif, setGif] = React.useState();
    const [text, setText] = React.useState('');

    const load = async () => {
        await ffmpeg.load();
        setReady(true);
    }

    useEffect(() => {
        load();
    }, [])

    const convertToGif = async () => {
        var tiempo = new Date();

        // Write the file to memory 
        ffmpeg.FS('writeFile', 'test.mp4', await fetchFile(video));
        ffmpeg.FS('writeFile', 'font.ttf', await fetchFile('./fonts/FreeSerifBoldItalic.ttf'));
        ffmpeg.FS('writeFile', 'font.ttf', await fetchFile('./fonts/FreeSerifBoldItalic.ttf'));
        // Run the FFMpeg command
        //await ffmpeg.run('-i', 'test.mp4',"-vf","drawgrid=w=iw/3:h=ih/3:t=2:c=white@0.5", '-q', '42', 'out.mp4');
        var styleVid = "drawtext=text="+text+":fontsize=30:fontfile=font.ttf:x=(w-text_w)/2:y=(h-text_h)/2, drawgrid=w=iw/3:h=ih/3:t=2:c=white";
        console.log(styleVid);
        await ffmpeg.run('-i', 'test.mp4', '-vf', styleVid, '-q', '42', 'out.mp4');
        // Read the result
        const data = ffmpeg.FS('readFile', 'out.mp4');

        // Create a URL
        const url = URL.createObjectURL(new Blob([data.buffer], { type: 'video/mp4' }));
        console.log('stop ' + (Math.abs(new Date() - tiempo)) / 1000 + ' seconds');

        setGif(url)
    }

    return ready ? (
        <div className="App">
            <div className='row'>

                <div className='col-md-6'>
                    <input type="file" className='mt-5 form-control' onChange={(e) => setVideo(e.target.files?.item(0))} />
                    <br></br>
                    <input type="text" className='form-control' onBlur={(e) => setText(e.target.value)} placeholder="Agregar Texto" />
                </div>

                <div className='col-md-6'>
                    <button className='mt-5 btn btn-warning' onClick={convertToGif}>Crear Video</button>


                </div>

            </div>
            <br></br>
            <hr></hr>
            <div className='row'>
                <div className='col-md'>
                    {video && <video width="500" className='mt-5'
                        controls
                        src={URL.createObjectURL(video)}>
                    </video>}
                </div>
                <div className='col-md'>
                    {gif && <video width="500" controls className='mt-5' src={gif} />}
                </div>
            </div>

        </div>
    ) :
        (
            <div>Cargando ffmpeg</div>
        )

}

export default VideoTest